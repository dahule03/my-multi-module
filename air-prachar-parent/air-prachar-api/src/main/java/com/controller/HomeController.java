package com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.air.model.WSModel;
import com.service.MyService;

@Controller
public class HomeController {
    
	@Autowired
	MyService myService;
	
	@RequestMapping(value = { "/", "/home" }, method = RequestMethod.GET)
    public String homePage(Model model) {
		WSModel ws=new WSModel();
		
		myService.m1();
		return "homePage";
    }
}
